This is a simple command line program that takes a .arff file as an argument
and returns a .html page that allows the user to examine three columns of the ARFF
file as a 3D plot.

Compilation:
g++ -Wall src/main.cpp src/Matrix.cpp -o arffplotter

Arguments:
-o - Specify the name of the output html file
-x - Specify the index (0-based) of the column to use for the x coordinates
-y - Specify the index (0-based) of the column to use for the y coordinates
-z - Specify the index (0-based) of the column to use for the z coordinates

Example usage:
arffplotter input.arff -o output.html -x 2 -y 3 -z 1

